'use strict';

var gulp = require('gulp'),
    less = require('gulp-less-sourcemap');

gulp.task('start', function() {
    return gulp.src('./public/css/less/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('./public/css/'));
});
